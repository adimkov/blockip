__author__ = 'Anton'

import re
import logging

log = logging.getLogger(__name__)

def parse_suspicious_ips(filename):
    """Parse suspicious IP addresses from apache access log file.

        @param filename: log file path
        @type filename: str

        @rtype: {str}
    """
    log_file = open(filename)
    with log_file:
        matches = re.finditer(
            r"(?P<ip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}).*(connect|php|cgi|bs)",
            log_file.read(),
            flags=re.I)
    ips = []
    for match in matches:
        log.debug("Found suspicious log line '%s'", match.group(0))
        ips.append(match.group("ip"))
    return set(ips)

def get_blocked_ips(filename):
    """Gets list of blocked ip address.

        @param filename: blocked IP file
        @type filename: str

        @rtype: {str}
    """
    block_ip_file = open(filename)
    with block_ip_file:
        matches = re.findall(r"(?P<ip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})", block_ip_file.read())
    return set(matches)

def append_to_blocked_ip(filename, ips_to_block):
    """Gets list of blocked ip address.

        @param filename: blocked IP file
        @type filename: str
        @param ips_to_block: IP addresses to block
        @type file: {str}
    """
    block_ip_file = open(filename, "a")
    with block_ip_file:
        block_ip_file.writelines(["\nRequire not ip %s" % line for line in ips_to_block])