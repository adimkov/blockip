__author__ = 'Anton'

import logparser
import logging
import subprocess

access_filename = r"c:\Server\logs\access.log"
blocked_ip_filename = r"c:\Server\conf\black_ip.cfg"
service_name = r"Apache2.4"

logging.basicConfig(filename=r"c:\Server\logs\block_ip.log",
                    format="%(asctime)s [%(levelname)s] [%(module)s] [%(process)d: %(thread)d] %(message)s",
                    level=logging.DEBUG)

log = logging.getLogger(__name__)

suspicious = logparser.parse_suspicious_ips(access_filename)
blocked = logparser.get_blocked_ips(blocked_ip_filename)
to_block = suspicious.difference(blocked)

log.info("Already blocked %s", blocked)
if to_block:
    log.info("Going to block: %s", to_block)
    log.info("Stop apache")
    logparser.append_to_blocked_ip(blocked_ip_filename, to_block)
    log.info("Blocked!")
    log.info("Starting apache")
    subprocess.call(["net", "stop", service_name], shell=True)
    subprocess.call(["net", "start", service_name], shell=True)
else:
    log.info("Nothing to block")
log.info("Done")
